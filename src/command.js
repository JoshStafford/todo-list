function executeCommand(comm) {

  comm = comm.substring(1);
  let terms = comm.split(" ");

  if(terms.length == 1) {

    genericCommand(comm);

  } else {

    if(argStart(terms) == 1){
      advCommand(terms);
    } else {
      complexCommand(terms);
    }

  }

}


function genericCommand(comm){

  switch(comm){

    case "clear-all":
      display("Clearing " + document.getElementsByClassName("todo-item").length + " todos");
      clearAll();

  }

}

function advCommand(terms){

  comm = terms[0];
  arg = findArg(terms);
  console.log(arg);

  switch(comm){

    case "title":
      list.setTitle(arg);
      break;
  }

}

function complexCommand(terms){

  let command = terms[0];
  let param = terms[1];
  let arg = findArg(terms);
  // arg = removeEC(arg);
  let targets = todoObjs;
  display("Finding targets");

  switch(param){
    case "--contains":
      targets = findContains(arg);
      break;

    case "--starts":
      targets = findStarts(arg);

    case "--name":
      var name = formatTarget(terms[2]);

  }


  switch(command){

    case "clear":
      clear(targets);
      display("Clearing " + targets.length + " todos");
      break;

    case "export":
      display("Exporting to file");
      exportList(name);

  }


}

// NEEDS FINISHING ASP
function removeEC(arg){

  let escape_char = false;
  result = "";

  for(var i = 0; i < arg.length; i++){

    if(arg[i] == "\\"){
      escape_char = true;
    }

  }

  return arg;

}

function findArg(terms){

  if(!Array.isArray(terms)){
      throw "Object not instance of array";
  }

  var start;
  var end;

  for(var i = 0; i < terms.length; i++){

    if(terms[i][0] == "\"" && !start){
      terms[i] = terms[i].substring(1);
      start = i;
    }

    if(terms[i][terms[i].length-1] == "\"" &&
              terms[i][terms[i].length-2] != "\\"){
      terms[i] = terms[i].substring(0, terms[i].length-1)
      end = i;
    }

  }

  return arrJoin(terms, start, end);

}


function findContains(target){


  result = [];

  target = formatTarget(target);

  for(var i = 0; i < list.todos.length; i++){
    current = list.todos[i];
    if(current.body.includes(target)){
      result.push(current);
    }
  }

  return result;

}

function findStarts(target){

  result = [];
  target = formatTarget(target);

  for(var i = 0; i < list.todos.length; i++){

    if(list.todos[i].body.split(" ")[0] == target){
      result.push(list.todos[i]);
    }

  }

  return result;

}


function formatTarget(target){

  if(target[0] == "\""){
    target = target.substring(1);
  }

  if(target[target.length-1] == "\""){
    target = target.substring(0, target.length-1);
  }

  return target;

}


function clearAll(){

  list.clear();

}

function clearItem(item){

  item.delete();

}

function clear(targets){

  for(var i = 0; i < targets.length; i++){
    targets[i].delete();
  }

}

function arrJoin(arr, start, end){

  if(start == end){
    return arr[start];
  }

  let result = "";

  for(var i = start; i < end; i++){

    result += String(arr[i]) + " ";

  }

  result += arr[arr.length-1];

  return result;

}

function exportList(name){

  if(!valid_name(name)){
    return false;
  }

}

function valid_name(name){

  console.log("Validating name.");
  file_name = name.split(".");
  if(file_name.includes(".")){
    display("File name should not include filetype");
    return false;
  }

}


function argStart(terms){

  for(term in terms){

    if(terms[term][0] == "\""){
      return term;
    }

  }

  return -1;

}
