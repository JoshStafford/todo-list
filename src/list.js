class List {

  constructor(title){

    this.todos = [];
    this.title = (!title)? "Todo list" : title;
    this.currentId = 0;

  }


  add(todo){

    this.todos.push(todo);
    todo.init();
    this.currentId++;
    this.update_page();

  }

  update_page(){

    todoList.innerText= "";

    for(todo in this.todos){

      todoList.appendChild(this.todos[this.todos.length - todo - 1].object);

    }

  }

  clear(){

    this.todos = [];
    this.update_page();

  }


  setTitle(title){

    this.title = title;

    let titleObj = document.getElementById("list-title");

    titleObj.innerText = title;
    display("Setting list title to \"" + title + "\"");

  }

  export(title){

    let data = "";

    data += this.title + "\n";

    for(todo in this.todos){

      data += "- " + todo.body + "\n";

    }

    let fileTitle = (!title) ? this.title : title;
    fileTitle += ".txt";

    fs.writeFile(fileTitle, data, err => {

      if(err) throw err;

    })

  }

}
