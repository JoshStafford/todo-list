class Todo {

  constructor(txt, index){

    this.body = txt;
    this.object = null;
    this.id = index;
    this.comp_button = null;

  }



  complete(){

    completedTodos.push(this);
    this.delete();

  }

  delete(){

    this.object.parentNode.removeChild(this.object);

    for(var i = 0; i < list.todos.length; i++){

      if(list.todos[i].equals(this)){
        list.todos.splice(i, 1);
      }

    }

  }

  equals(other){

    return this.body == other.body && this.object == other.object;

  }

  // Add todo to xml file
  init(){


    let result = document.createElement("div");
    result.classList.add("todo-item");
    result.id = list.currentId + "_todo";


    let completeButton = document.createElement("BUTTON");
    completeButton.innerText = "Complete";
    completeButton.classList.add("complete-button");
    completeButton.style.display = "none";
    this.comp_button = completeButton;
    result.appendChild(completeButton);

    result.addEventListener("mouseover", function(){

      completeButton.style.display = "block";

    })


    result.addEventListener("mouseout", function(){

      completeButton.style.display = "none";

    })

    completeButton.onclick = e => {

      this.complete();

    }

    let todoText = document.createElement("P");
    todoText.classList.add("todo-text");
    todoText.innerText = this.body;
    result.appendChild(todoText);

    let breakline = document.createElement("HR");
    result.appendChild(breakline);

//    todoList.appendChild(result);
//    todo_no += 1;

    this.object = result;
//    todoObjs.push(this);
//    result.classList.add("fade-in") ;

  }



}
