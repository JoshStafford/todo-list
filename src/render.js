const consoleLog = document.getElementById("console-text");
const todoList = document.getElementById("todo-list");
const todoInput = document.getElementById("todo-input");
let todo_no = 0;
let list = new List();
let todoObjs = [];
let completedTodos = [];
let commHistory = [];

todoInput.onkeydown = e => {

  let inp = todoInput.value;

  while(inp[0] == " "){
      inp = inp.subsbstring(1);
  }

  if(e.key == 'Enter'){
    if(!todoInput.value){
      display("Please enter an input.");
    }
    else if(inp[0] == "!"){
      executeCommand(inp);
    } else {

      todo = new Todo(inp, list.currentId);
      list.add(todo);

    }
    todoInput.value = "";
  }

}


function display(text){

  console.log(text);

  commHistory.push(text);

  consoleLog.innerText = "> " + text;

}

function typewrite(letter){

  consoleLog.innerText += letter;

}


function addNewTodo(txt){

  todo = new Todo(txt);
  todo.init();

}
